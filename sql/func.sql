drop  FUNCTION if exists CallDolphinschedulerProcessFn(string,string,string,string,string);
CREATE FUNCTION CallDolphinschedulerProcessFn(string,string,string,string,string) RETURNS String PROPERTIES (
    "file"="http://10.110.175.116/doris-call-dolphinscheduler-process-1.0-SNAPSHOT.jar",
    "symbol"="dev.doris.CallDolphinschedulerProcessFn",
    "always_nullable"="true",
    "type"="JAVA_UDF"
);